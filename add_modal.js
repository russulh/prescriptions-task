import React, {Component} from 'react';

class AddModal extends Component {

    render() {

        return (
            <div className={this.state.modalClass}>
                <div className="modal-background"></div>
                <div className="modal-content">
                    <div className="field">
                        <label className="label">Name</label>
                        <div className="control">
                            <input className="input" type="text" placeholder="e.g Alex Smith">
                        </div>
                    </div>

                    <div className="field">
                        <label className="label">Email</label>
                        <div className="control">
                            <input className="input" type="email" placeholder="e.g. alexsmith@gmail.com"/>
                        </div>
                    </div>
                </div>
                <button className="modal-close is-large" aria-label="close"
                        onClick={this.toggleModal.bind(this, 'close')}></button>
            </div>
        )
    }
}


export default AddModal;