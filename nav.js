import React, {Component} from 'react';
import Context from "./Context";
import Select from 'react-select';

// const options = [
//     { value: 'chocolate', label: 'Chocolate' },
//     { value: 'strawberry', label: 'Strawberry' },
//     { value: 'vanilla', label: 'Vanilla' }
// ];

class Nav extends Component {
    constructor() {
        super();
        // console.log('props', props);
        this.state = {
            selectedOption: [],
            name: '',
            age: '',
            searchValue: '',
            searchType: 'name',
            inputType: 'text'
        };
    }

    handleChange(new_value) {
        this.setState({
            selectedOption: new_value
        });
        // console.log(`Option selected:`, new_value);
    }

    bindState(type, e) {
        let new_value = e.target.value;
        if (type === 'name') {
            this.setState({
                name: new_value
            });
        } else if (type === 'age') {
            this.setState({
                age: new_value
            });
        } else if (type === 'search') {
            this.setState({
                searchValue: new_value
            });
        } else if (type === 'searchType') {
            let input_type;
            if(new_value === 'age'){
                input_type = 'number';
            }
            else if (new_value === 'date'){
                input_type = 'date';
            }
            else{
                input_type = 'text';
            }

            this.setState({
                searchType: new_value,
                inputType: input_type
            });
        }
    }

    render() {
        const {selectedOption} = this.state.selectedOption;

        return (
            <Context.Consumer>
                {
                    (ctx) => {
                        let drugs = ctx.state.drugs;
                        // console.log('drugs = ',drugs);
                        return (
                            <nav className="navbar is-light is-fixed-top" role="navigation" aria-label="main navigation">
                                <div className="navbar-brand">
                                    <a className="navbar-item" href="/">
                                        <img
                                            src="data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHhtbG5zOnhsaW5rPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5L3hsaW5rIiB2aWV3Qm94PSIwIDAgMTYzLjUxMyA0My44OTMiPiAgPGRlZnM+ICAgIDxzdHlsZT4gICAgICAuY2xzLTEgeyAgICAgICAgZmlsbDogIzMwM2M0MjsgICAgICB9ICAgICAgLmNscy0yIHsgICAgICAgIGZpbGw6ICNkZmUxZGY7ICAgICAgfSAgICAgIC5jbHMtMyB7ICAgICAgICBmaWxsOiAjMDBhYWU3OyAgICAgIH0gICAgICAuY2xzLTQgeyAgICAgICAgZmlsbDogdXJsKCNsaW5lYXItZ3JhZGllbnQpOyAgICAgIH0gICAgICAuY2xzLTUgeyAgICAgICAgZmlsbDogIzQ1NTA1NjsgICAgICAgIGZvbnQtc2l6ZTogMjJweDsgICAgICAgIGZvbnQtZmFtaWx5OiBIb2JvU3RkLCBIb2JvIFN0ZDsgICAgICB9ICAgIDwvc3R5bGU+ICAgIDxsaW5lYXJHcmFkaWVudCBpZD0ibGluZWFyLWdyYWRpZW50IiB4MT0iLTAuMDI5IiB5MT0iMC4xNzEiIHgyPSIxLjAyOSIgeTI9IjAuODI5IiBncmFkaWVudFVuaXRzPSJvYmplY3RCb3VuZGluZ0JveCI+ICAgICAgPHN0b3Agb2Zmc2V0PSIwIiBzdG9wLWNvbG9yPSIjZmZmIiBzdG9wLW9wYWNpdHk9IjAuMiIvPiAgICAgIDxzdG9wIG9mZnNldD0iMSIgc3RvcC1jb2xvcj0iI2ZmZiIgc3RvcC1vcGFjaXR5PSIwIi8+ICAgIDwvbGluZWFyR3JhZGllbnQ+ICA8L2RlZnM+ICA8ZyBpZD0iR3JvdXBfMSIgZGF0YS1uYW1lPSJHcm91cCAxIiB0cmFuc2Zvcm09InRyYW5zbGF0ZSgtMy40ODggLTIpIj4gICAgPGcgaWQ9Imljb25maW5kZXJfZHJ1Z3NfZHJ1Z19hZGRpZ3Rpb25fUGlsbHNfMV8zMTIyNDE3IiB0cmFuc2Zvcm09Im1hdHJpeCgwLjY1NiwgMC43NTUsIC0wLjc1NSwgMC42NTYsIDI1LjY3OSwgMC4wMzIpIj4gICAgICA8cGF0aCBpZD0iUGF0aF8xIiBkYXRhLW5hbWU9IlBhdGggMSIgY2xhc3M9ImNscy0xIiBkPSJNMzIuMjcyLDNIMi45MzRBMi45MzcsMi45MzcsMCwwLDAsMCw1LjkzNFYyNi40NzFBMi45MzcsMi45MzcsMCwwLDAsMi45MzQsMjkuNEgzMi4yNzJhMi45MzcsMi45MzcsMCwwLDAsMi45MzQtMi45MzRWNS45MzRBMi45MzcsMi45MzcsMCwwLDAsMzIuMjcyLDNaIi8+ICAgICAgPHBhdGggaWQ9IlBhdGhfMiIgZGF0YS1uYW1lPSJQYXRoIDIiIGNsYXNzPSJjbHMtMiIgZD0iTTMzLjI3MiwyNmExLjQ2OCwxLjQ2OCwwLDAsMS0xLjQ2NywxLjQ2N0gyLjQ2N0ExLjQ2OCwxLjQ2OCwwLDAsMSwxLDI2VjUuNDY3QTEuNDY4LDEuNDY4LDAsMCwxLDIuNDY3LDRIMzEuODA1YTEuNDY4LDEuNDY4LDAsMCwxLDEuNDY3LDEuNDY3WiIgdHJhbnNmb3JtPSJ0cmFuc2xhdGUoMC40NjcgMC40NjcpIi8+ICAgICAgPHBhdGggaWQ9IlBhdGhfMyIgZGF0YS1uYW1lPSJQYXRoIDMiIGNsYXNzPSJjbHMtMSIgZD0iTTI4LjY3MSwxMS41SDMuNzMzYS43MzMuNzMzLDAsMSwwLDAsMS40NjdIMjguNjcxYS43MzMuNzMzLDAsMCwwLDAtMS40NjdaIiB0cmFuc2Zvcm09InRyYW5zbGF0ZSgxLjQwMSAzLjk2OSkiLz4gICAgICA8cGF0aCBpZD0iUGF0aF80IiBkYXRhLW5hbWU9IlBhdGggNCIgY2xhc3M9ImNscy0xIiBkPSJNNS4yLDEyLjMzNWEyLjIsMi4yLDAsMCwwLDIuMi0yLjJWNy4yQTIuMiwyLjIsMCwxLDAsMyw3LjJ2Mi45MzRBMi4yLDIuMiwwLDAsMCw1LjIsMTIuMzM1WiIgdHJhbnNmb3JtPSJ0cmFuc2xhdGUoMS40MDEgMC45MzQpIi8+ICAgICAgPHBhdGggaWQ9IlBhdGhfNSIgZGF0YS1uYW1lPSJQYXRoIDUiIGNsYXNzPSJjbHMtMyIgZD0iTTQsNi43MzNhLjczMy43MzMsMCwwLDEsMS40NjcsMFY5LjY2N0EuNzMzLjczMywwLDEsMSw0LDkuNjY3WiIgdHJhbnNmb3JtPSJ0cmFuc2xhdGUoMS44NjggMS40MDEpIi8+ICAgICAgPHBhdGggaWQ9IlBhdGhfNiIgZGF0YS1uYW1lPSJQYXRoIDYiIGNsYXNzPSJjbHMtMSIgZD0iTTIwLjIsNUEyLjIsMi4yLDAsMCwwLDE4LDcuMnYyLjkzNGEyLjIsMi4yLDAsMCwwLDQuNCwwVjcuMkEyLjIsMi4yLDAsMCwwLDIwLjIsNVoiIHRyYW5zZm9ybT0idHJhbnNsYXRlKDguNDA1IDAuOTM0KSIvPiAgICAgIDxwYXRoIGlkPSJQYXRoXzciIGRhdGEtbmFtZT0iUGF0aCA3IiBjbGFzcz0iY2xzLTMiIGQ9Ik0yMC40NjcsOS42NjdhLjczMy43MzMsMCwxLDEtMS40NjcsMFY2LjczM2EuNzMzLjczMywwLDEsMSwxLjQ2NywwWiIgdHJhbnNmb3JtPSJ0cmFuc2xhdGUoOC44NzEgMS40MDEpIi8+ICAgICAgPHBhdGggaWQ9IlBhdGhfOCIgZGF0YS1uYW1lPSJQYXRoIDgiIGNsYXNzPSJjbHMtMSIgZD0iTTE1LjIsMTIuMzM1YTIuMiwyLjIsMCwwLDAsMi4yLTIuMlY3LjJhMi4yLDIuMiwwLDEsMC00LjQsMHYyLjkzNEEyLjIsMi4yLDAsMCwwLDE1LjIsMTIuMzM1WiIgdHJhbnNmb3JtPSJ0cmFuc2xhdGUoNi4wNyAwLjkzNCkiLz4gICAgICA8cGF0aCBpZD0iUGF0aF85IiBkYXRhLW5hbWU9IlBhdGggOSIgY2xhc3M9ImNscy0zIiBkPSJNMTQsNi43MzNhLjczMy43MzMsMCwxLDEsMS40NjcsMFY5LjY2N2EuNzMzLjczMywwLDEsMS0xLjQ2NywwWiIgdHJhbnNmb3JtPSJ0cmFuc2xhdGUoNi41MzcgMS40MDEpIi8+ICAgICAgPHBhdGggaWQ9IlBhdGhfMTAiIGRhdGEtbmFtZT0iUGF0aCAxMCIgY2xhc3M9ImNscy0xIiBkPSJNMTAuMiwxMi4zMzVhMi4yLDIuMiwwLDAsMCwyLjItMi4yVjcuMkEyLjIsMi4yLDAsMSwwLDgsNy4ydjIuOTM0QTIuMiwyLjIsMCwwLDAsMTAuMiwxMi4zMzVaIiB0cmFuc2Zvcm09InRyYW5zbGF0ZSgzLjczNSAwLjkzNCkiLz4gICAgICA8cGF0aCBpZD0iUGF0aF8xMSIgZGF0YS1uYW1lPSJQYXRoIDExIiBjbGFzcz0iY2xzLTMiIGQ9Ik05LDYuNzMzYS43MzMuNzMzLDAsMSwxLDEuNDY3LDBWOS42NjdBLjczMy43MzMsMCwwLDEsOSw5LjY2N1oiIHRyYW5zZm9ybT0idHJhbnNsYXRlKDQuMjAyIDEuNDAxKSIvPiAgICAgIDxwYXRoIGlkPSJQYXRoXzEyIiBkYXRhLW5hbWU9IlBhdGggMTIiIGNsYXNzPSJjbHMtMSIgZD0iTTUuMiwxNEEyLjIsMi4yLDAsMCwwLDMsMTYuMnYyLjkzNGEyLjIsMi4yLDAsMCwwLDQuNCwwVjE2LjJBMi4yLDIuMiwwLDAsMCw1LjIsMTRaIiB0cmFuc2Zvcm09InRyYW5zbGF0ZSgxLjQwMSA1LjEzNikiLz4gICAgICA8cGF0aCBpZD0iUGF0aF8xMyIgZGF0YS1uYW1lPSJQYXRoIDEzIiBjbGFzcz0iY2xzLTMiIGQ9Ik01LjQ2NywxOC42NjdhLjczMy43MzMsMCwwLDEtMS40NjcsMFYxNS43MzNhLjczMy43MzMsMCwwLDEsMS40NjcsMFoiIHRyYW5zZm9ybT0idHJhbnNsYXRlKDEuODY4IDUuNjAzKSIvPiAgICAgIDxwYXRoIGlkPSJQYXRoXzE0IiBkYXRhLW5hbWU9IlBhdGggMTQiIGNsYXNzPSJjbHMtMSIgZD0iTTIwLjIsMTRBMi4yLDIuMiwwLDAsMCwxOCwxNi4ydjIuOTM0YTIuMiwyLjIsMCwwLDAsNC40LDBWMTYuMkEyLjIsMi4yLDAsMCwwLDIwLjIsMTRaIiB0cmFuc2Zvcm09InRyYW5zbGF0ZSg4LjQwNSA1LjEzNikiLz4gICAgICA8cGF0aCBpZD0iUGF0aF8xNSIgZGF0YS1uYW1lPSJQYXRoIDE1IiBjbGFzcz0iY2xzLTMiIGQ9Ik0yMC40NjcsMTguNjY3YS43MzMuNzMzLDAsMCwxLTEuNDY3LDBWMTUuNzMzYS43MzMuNzMzLDAsMCwxLDEuNDY3LDBaIiB0cmFuc2Zvcm09InRyYW5zbGF0ZSg4Ljg3MSA1LjYwMykiLz4gICAgICA8cGF0aCBpZD0iUGF0aF8xNiIgZGF0YS1uYW1lPSJQYXRoIDE2IiBjbGFzcz0iY2xzLTEiIGQ9Ik0xNS4yLDE0QTIuMiwyLjIsMCwwLDAsMTMsMTYuMnYyLjkzNGEyLjIsMi4yLDAsMCwwLDQuNCwwVjE2LjJBMi4yLDIuMiwwLDAsMCwxNS4yLDE0WiIgdHJhbnNmb3JtPSJ0cmFuc2xhdGUoNi4wNyA1LjEzNikiLz4gICAgICA8cGF0aCBpZD0iUGF0aF8xNyIgZGF0YS1uYW1lPSJQYXRoIDE3IiBjbGFzcz0iY2xzLTMiIGQ9Ik0xNS40NjcsMTguNjY3YS43MzMuNzMzLDAsMCwxLTEuNDY3LDBWMTUuNzMzYS43MzMuNzMzLDAsMCwxLDEuNDY3LDBaIiB0cmFuc2Zvcm09InRyYW5zbGF0ZSg2LjUzNyA1LjYwMykiLz4gICAgICA8cGF0aCBpZD0iUGF0aF8xOCIgZGF0YS1uYW1lPSJQYXRoIDE4IiBjbGFzcz0iY2xzLTEiIGQ9Ik0xMC4yLDE0QTIuMiwyLjIsMCwwLDAsOCwxNi4ydjIuOTM0YTIuMiwyLjIsMCwwLDAsNC40LDBWMTYuMkEyLjIsMi4yLDAsMCwwLDEwLjIsMTRaIiB0cmFuc2Zvcm09InRyYW5zbGF0ZSgzLjczNSA1LjEzNikiLz4gICAgICA8cGF0aCBpZD0iUGF0aF8xOSIgZGF0YS1uYW1lPSJQYXRoIDE5IiBjbGFzcz0iY2xzLTMiIGQ9Ik0xMC40NjcsMTguNjY3YS43MzMuNzMzLDAsMCwxLTEuNDY3LDBWMTUuNzMzYS43MzMuNzMzLDAsMCwxLDEuNDY3LDBaIiB0cmFuc2Zvcm09InRyYW5zbGF0ZSg0LjIwMiA1LjYwMykiLz4gICAgICA8cGF0aCBpZD0iUGF0aF8yMCIgZGF0YS1uYW1lPSJQYXRoIDIwIiBjbGFzcz0iY2xzLTQiIGQ9Ik0zMi4yNzIsM0gyLjkzNEEyLjkzNywyLjkzNywwLDAsMCwwLDUuOTM0VjI2LjQ3MUEyLjkzNywyLjkzNywwLDAsMCwyLjkzNCwyOS40SDMyLjI3MmEyLjkzNywyLjkzNywwLDAsMCwyLjkzNC0yLjkzNFY1LjkzNEEyLjkzNywyLjkzNywwLDAsMCwzMi4yNzIsM1oiLz4gICAgPC9nPiAgICA8dGV4dCBpZD0iUHJlc2NyaXB0aW9uIiBjbGFzcz0iY2xzLTUiIHRyYW5zZm9ybT0idHJhbnNsYXRlKDQ1IDMyKSI+PHRzcGFuIHg9IjAiIHk9IjAiPlByZXNjcmlwdGlvbjwvdHNwYW4+PC90ZXh0PiAgPC9nPjwvc3ZnPg=="
                                            width="112" height="28"/>
                                    </a>

                                    <a role="button" className="navbar-burger burger" aria-label="menu"
                                       aria-expanded="false"
                                       data-target="navbarBasicExample">
                                        <span aria-hidden="true"></span>
                                        <span aria-hidden="true"></span>
                                        <span aria-hidden="true"></span>
                                    </a>
                                </div>

                                <div id="navbarBasicExample" className="navbar-menu">
                                    <div className="navbar-end">
                                        <div className="navbar-item">
                                            <div className="field has-addons">
                                                <p className="control">
                                                    <span className="select">
                                                      <select onChange={this.bindState.bind(this, 'searchType')}>
                                                        <option value="name">Name</option>
                                                        <option value="age">Age</option>
                                                        <option value="date">Date</option>
                                                      </select>
                                                    </span>
                                                </p>
                                                <p className="control">
                                                    <input className="input" type={this.state.inputType}
                                                           onChange={this.bindState.bind(this, 'search')}
                                                           placeholder="Find a prescription"/>
                                                </p>
                                                <p className="control">
                                                    <button className="button"
                                                            onClick={ctx.actions.findPre.bind(this, this.state.searchType, this.state.searchValue)}>Search
                                                    </button>
                                                </p>
                                            </div>
                                        </div>
                                        <div className="navbar-item">
                                            <div className="buttons">
                                                <a className="button is-primary modal-button" data-target="modal"
                                                   aria-haspopup="true"
                                                   onClick={ctx.actions.toggleModal.bind(this, 'open')}
                                                >Add</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div className={ctx.state.modalClass}>
                                    <div className="modal-background"></div>
                                    <div className="modal-content">
                                        <div className="box">
                                            <div className="field">
                                                <label className="label">Name</label>
                                                <div className="control">
                                                    <input className="input" type="text" placeholder="Enter Name"
                                                           onKeyUp={this.bindState.bind(this, 'name')}/>
                                                </div>
                                            </div>

                                            <div className="field">
                                                <label className="label">Age</label>
                                                <div className="control">
                                                    <input className="input" type="number" placeholder="Enter Age"
                                                           onKeyUp={this.bindState.bind(this, 'age')}/>
                                                </div>
                                            </div>

                                            <div className="field">
                                                <label className="label">Drugs</label>
                                                <div className="is-fullwidth">
                                                    <Select
                                                        value={selectedOption}
                                                        onChange={this.handleChange.bind(this)}
                                                        options={drugs} isMulti
                                                    />
                                                </div>
                                            </div>

                                            <div className="control">
                                                <button className="button is-primary is-fullwidth"
                                                        onClick={ctx.actions.addPre.bind(this, this.state)}>Submit
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                    <button className="modal-close is-large" aria-label="close"
                                            onClick={ctx.actions.toggleModal.bind(this, 'close')}
                                            onKeyUp={ctx.actions.toggleModal.bind(this, 'close')}></button>
                                </div>
                            </nav>
                        )
                    }
                }
            </Context.Consumer>
        )
    }
}

export default Nav;