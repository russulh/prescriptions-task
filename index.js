import React from 'react';
import ReactDOM from 'react-dom';
import 'bulma/css/bulma.css';
import firebase from 'firebase';
import moment from 'moment';
import pdfMake from 'pdfmake/build/pdfmake';
import pdfFonts from "pdfmake/build/vfs_fonts";

pdfMake.vfs = pdfFonts.pdfMake.vfs;

import Context from '/Context';
import Nav from '/nav';
import List from '/list';

let config = {
    apiKey: "AIzaSyAh4MEkzwhu-Ka-phxqdZ8AkE-u5UpgCg8",
    authDomain: "prescription-727c2.firebaseapp.com",
    databaseURL: "https://prescription-727c2.firebaseio.com",
    projectId: "prescription-727c2",
    storageBucket: "prescription-727c2.appspot.com",
    messagingSenderId: "1081926093945"
};
firebase.initializeApp(config);

const firestore = firebase.firestore();
const settings = {timestampsInSnapshots: true};
firestore.settings(settings);

class App extends React.Component {
    constructor() {
        super();
        this.state = {
            drugs: [],
            data: [
                // {age: "23", date: "23-20-200", drugs: Array(1), name: "russul"},
                // {age: "89", date: "23-20-200", drugs: Array(1), name: "chiku"}
            ],
            modalFlag: false,
            modalClass: 'modal'
        };
        // console.log('local',this.state.data);
        // firebase.firestore().collection('prescriptions')
        //     .orderBy('date', 'desc')
        //     .onSnapshot((snapshot) => {
        //         let pres = [];
        //         snapshot.forEach((doc) => {
        //             pres.push(doc.data())
        //         });
        //         // console.log('pre',pres);
        //         this.setState({
        //             data: pres
        //         });
        //     });
        // console.log('fr',this.state.data);
        // if (this.state.drugs.length === 0) {
        //     this.getDrugsFromFirebase();
        // }
    }

    componentDidMount() {
        this.getAllPrescriptions();
        // firebase.firestore().collection('prescriptions')
        //     .orderBy('date', 'desc')
        //     .onSnapshot((snapshot) => {
        //         let pres = [];
        //         snapshot.forEach((doc) => {
        //             pres.push(doc.data())
        //         });
        //         this.setState({
        //             data: pres
        //         });
        //     });

        // if (this.state.drugs.length === 0) {
        //     this.getDrugsFromFirebase();
        // }
    }

    getAllPrescriptions(){
        firebase.firestore().collection('prescriptions')
            .orderBy('date', 'desc')
            .onSnapshot((snapshot) => {
                let pres = [];
                snapshot.forEach((doc) => {
                    pres.push(doc.data())
                });
                this.setState({
                    data: pres
                });
            });
    }
    getDrugsFromFile() {
        console.log('file');
        let data = require('/data.json');
        let drugs = [];
        console.log(data.results.length);
        data.results.map((item, i) => {
            if (i < 100) {
                firebase.firestore().collection('drugs').add({
                    label: item.term.toLowerCase(),
                    value: item.term.toLowerCase(),
                    date: Date.now()
                });

                drugs.push({
                    name: item.term.toLowerCase()
                });
            }
        });
        this.setState({
            drugs: drugs
        })
    }

    getDrugsFromFirebase() {
        // console.log('firebase');
        firebase.firestore().collection('drugs')
            .orderBy('label', 'asc')
            .onSnapshot((snapshot) => {
                console.log(snapshot.size);
                let drugs = [];
                if (snapshot.size > 0) {
                    snapshot.forEach((doc) => {
                        drugs.push(doc.data())
                    });
                    this.setState({
                        drugs: drugs
                    });
                } else {
                    this.getDrugsFromFile();
                }
            })
    }

    toggleModal(type) {
        let toggle;
        let modal_class;

        if (type === 'open') {
            toggle = true;
            modal_class = 'modal is-active';
            if (this.state.drugs.length === 0) {
                this.getDrugsFromFirebase();
            }
        } else {
            toggle = false;
            modal_class = 'modal';
        }

        this.setState({
            modalFlag: toggle,
            modalClass: modal_class
        })
    }

    findPrescription(type, value) {
        console.log('bbo', type, typeof(value), value);
        let pres = [];
        let holder = this;
        if(value === ''){
            this.getAllPrescriptions();
        }
        else {
            if (type === 'date') {
                let value_start = moment(value).startOf('day');
                let value_end = moment(value).endOf('day');

                firebase.firestore().collection("prescriptions")
                    .where(type, '>=', new Date(value_start))
                    .where(type, '<=', new Date(value_end))
                    .get()
                    .then(function (querySnapshot) {
                        if (querySnapshot.size > 0) {
                            querySnapshot.forEach(function (doc) {
                                pres.push(doc.data());
                                // console.log(doc.id, " => ", doc.data());
                            });

                            holder.setState({
                                data: pres
                            });

                        } else {
                            alert('No results, try again .... :)');
                        }
                    })
                    .catch(function (error) {
                        console.log("Error getting documents: ", error);
                    });
            } else {
                let op = '==';
                // if(type === 'name'){
                //     op = 'contains';
                // }

                firebase.firestore().collection("prescriptions")
                    .where(type, op, value)
                    .get()
                    .then(function (querySnapshot) {
                        if (querySnapshot.size > 0) {
                            querySnapshot.forEach(function (doc) {
                                pres.push(doc.data());
                            });

                            holder.setState({
                                data: pres
                            });

                        } else {
                            alert('No results, try again .... :)');
                        }
                    })
                    .catch(function (error) {
                        console.log("Error getting documents: ", error);
                    });
            }
        }
        // holder.setState({
        //     data: pres
        // });
    }

    addPrescription(data){
        if (!data.name || !data.age || data.selectedOption.length === 0) {
            alert('Please fill all inputs');
            return false;
        }
        else if(data.name.length > 100){
            alert('Please name should be less 100 characters');
            return false;
        }
        else if(data.age > 200){
            alert('Please age should be less 200 years old');
            return false;
        }
        firebase.firestore().collection('prescriptions').add({
            date: new Date(),
            name: data.name,
            age: data.age,
            drugs: data.selectedOption
        });

        alert('Add new prescription');
        this.toggleModal('close');
    }
    render() {
        return (
            <Context.Provider value={{
                state: this.state,
                actions: {
                    toggleModal: (type) => {
                        this.toggleModal(type);
                    },
                    addPre: (data) => {
                        // console.log(data);
                        this.addPrescription(data);
                    },
                    exportPDF: (item) => {
                        // console.log(item);
                        var pdf_page = {
                            content: [
                                {
                                    image: "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAqIAAACtCAYAAABryB0PAAAACXBIWXMAAC4jAAAuIwF4pT92AAAgAElEQVR4nO2dB3gU5dbHz+wmkGANeL8rBBAp9krovQQQqaEk9E4oiqCgYL12wIIFpASQLiVSFKQGEWmixF6okRLAeyWJjSSUZL/nTN7FNcz7Tp+d3Zxfnn0CmfZO2Zn/nCr5fD4g+CT2H3K/5PF0lSTpDo/HU+fSjD7fqUKfL7OwoGDtxYKC5asWzztEh5EgCIIgCEI7JEQ5JPYf0jsiIuIVkKQKWuYvKChYeeHChcdJkBIEQRAEQWiDhGgxEvoMLFcqMnKW1+vtqnthn++vixcvPr5iwZxpNg+TIAiCIAgi5CEhGgCK0OjSpXeBJN1sZj0XL1wYRWKUIAiCIAhCjIeOTxGJA4bWiy5d+luzIhSJiIycmth/yIMWDY0gCIIgCCIsIYsoS0iKiIhYDpJ0pZXrvXjhQp8VC+YssXKdBEEQBEEQ4UKJF6JouYyIiJgqmicqKgpq17wH7rztZoiJuRby8/MhO+c3+OLLr+H7Hw9wl/P5fH+dv3AhfuWid/faMXaCIAiCIIhQpkQL0cQBQ1+L8HrHiuapUP56GDmkP0RFlVacnvHzMXh38XJZnCqBYvTc+fM1KZueIAiCIAjin5RYIZo4YOj7ESqZ8bVr3g1JXTupruv0L/+F16fO4k4v9PkOnDt/vuHqxfOyjI2WIAiCIAgi/IgoaecUM+NLlyq1PsLrrVN8WqAobxPfDFq3aKppneWv/zf06NoJlr6/RnG6BHBzqcjI9QBQ18zYCYIgCIIgwokSlTXfpc/AGqUiI3dKAHVQdBb/+OnZrbNmEeqnVs27oXP7+7jTPZJUB62wFuwGQRAEQRBEWFBihGjXvoPqlipV6kuPJN3Cmyc6OgpGDh0gi0ojNG5QF2rH3cNd0uvxdMW4VFt2kCAIgiAIIsQoEUK0W7/BvSIjI9MkAG55Jr8IrXbjDaa2hS76O267GdC+qvTxeDxju/Ub/ICpjRAEQRAEQYQBYZ+shKIvIiJC2OUotvz18MDQAdzMeL3k55+Dd2bPh5Onf+EueeHChftXLnp3gyUbJAiCIAiCCEHCWogm9h+S6vF6u4nmwdqgPbslWCZC/aAYfeGVNyFPUNbp4sWLVGOUIAiCIIgSS1gKUcyMj/R6Z6qJ0Do174Ye3TrbNo5Tp3+BaSnzRWL01IWLF++isk4EQRAEQZREwk6IMhG6U/J4uElJ8nzt74MmDevZPp4jGUdh2uz53Om+wsL9FwoKGpEYJQiCIAiipBFWyUqYGa9FhPbqnuCICEWqVa0ib48HjjXC613qyGAIgiAIgiBcRNhYRFGEer3eNEmS+JnxUVHwYPIAuW2n06xZtxG27/qMu9WCgoL33184t7vjAyMIgiAIgggSYWERZZnxnymJUH/RehSfsgi9/nrluko2fzq3u09uGapUSB8/Ho+nW5c+A6msE0EQBEEQJYaQt4h26TPwyYiIiBdF88RWKA+j5PJMUZZsMzsnB7J/+13+jZSNiZFLQEVrWP+rU2fCyVOnudMvXrz44KrF896xZKAEQRAEQRAuJqSFaLd+g1PRkiiap07cPdClfVtLROh3P+6H7bv2wOGMo4rT77ztFmjaqD5Uv7EKdx35+fkwFWuM8sSoz/dXQWEhlXUiCIIgCCLsCUkhipnxmOAjSVIr0XxNG9aXs+PNkpefB0tWrIHvf/xJ05qaNKoHXdrfz52e/dtv8OpbMyAvL09xug/gzwsXLsSveW/B56YHTxAEQRAE4VJCTogyEbpTAhBnxid2gTo1+X3ftYLu9zkLl8o1QfWAPed7d+/CXeLkL7/AtFnzRGJ0/0Uq60QQBEEQRBgTUslKnXv1rxPh9f4sEqHR0dEwuF9PS0Ro5unT8MrbM3SLUOSL9K9h1br13Omx118PCR3acqfjPqLg1r1hgiAIgiCIECFkhGhC7wH3RUZGpkkAV/HmQRH64LCBcOdtt5re3rc//ATTUuZBft7lXZG0Jthv3/kZZLGEJiVQLKPllrc8ANzSte+gFaZ3hiAIgiAIwoWEhBDFskYREREbAOAqnmjD8kyPjh4hWxrNsjf9K5i7aCnk5eXzBCIUREZB7r+ryh8RG7dsE05HMYoJVTw8Hk/3Ln0Gzrbz+BIEQRAEQQQD18eIogjzer1DRPNUr1oFhvTrpal8khqr1q4XFp5H/qx0O5yu3x0KI4u2F3n2N4jdvgCici7PhI+KKg2Tn31SdbtLUlfB5+lfc6cXFBRQWSeCIAiCIMIKV1tE0S2tJkLRmjgqeZBpEZorZ8avUhWhv1eNg5NN+l4SociFK66F/8V1UJwfraronvep/HTpcL9s1RUUvJ/WqWe/nqZ2kiAIgiAIwkW40iKKmfFej2enJEmKSUn+Mbdt1QLui29ment5WNtz5rtwUiUp6b9xHSDnlkbc6bcsGa/491HDBslWWzXy88/B5Dffgeyc3xTn9Pl8fxYUFFBZJ4IgCIIgwgLXWUQ79Ohb3SNJO1DX8ayDSO/ELpaIUCwsr0WEnq6fKBShVoBu/KH9+SEGkiRdhf30UajbOhCCIAiCIAgHcJUQxfJMpSIjv5QkiZv2jiLtoWGDhAk+WkER+vYssQgtKBUFP7cbI7vkRZT5XwZ3amwF7QlU6J5/aPggroseE7YkgB0kRgmCIAiCCHVcI0Q79+o/Eq19aPXjzVM25lpZpFXT4OZWA93xsxe8V5QZzxF9+THl4Xj8cMi/trywTFPpnNMQ+8l8xXXEXHuN7vhVFKN9krpyp6NQlwA+Mn0QCIIgCIIggogrhCgToe/I5Zk4ojC2/PUwfswDskizgk927ObGYiLnylaA462Gy2JUxDUZ6VB5ywzwnL+83ihSt9a9hkaLFt/7W7XgTpckqW5C7wFUY5QgCIIgiJAl6EIUxRQToVzuuuNWeGj4YDmG0grQGrptxx7umn6vWksWoQWRYksmitDyu5dzRWhUVBQ0a9zA8Ijva9WcK2RRnEuS1L1zr/5PGN4AQRAEQRBEEIkI1qblGEefbwYWbBfNh0IME5Os5NCRnyE//5/i0Z8E9Xu1WnC6QaLq1lCAXnNkH4hqDnTteL/pslK471nZOfKYlfB4PC917Nkv58OlC2dYeIgIgiAIgiBsJygWUSZCd6BFj+eKxw/W1rRahCKZJ08rZuKjO/50gyThst4LeVBx+wJZhIpAAW3ULV+c5P59oGKFCtzpEV7vdEz0smRjBEEQBEEQDuG4EGWC6RtRZnyZ6Gjo26MrNGtc39Gx/VnpDuF0FKGVN8+EK49/L5wPE41EyUZ6iYouDaOHD5aPCw+Px5NGYpQgCIIgiFDCUSGKQgkFkwQQy5sHxdZDIwZDnThrrIlWge07q6x7E0pnn+Jmz2MMK1pwrbKEBoJiFONk0dXP2f5VkiStorJOBEEQBEGECo4J0Y49+43weDx7MTNeabpPrrdZXhZbsRZlxvPxKYq5qw9/AeilL/5B8Vlp03SI+CubGxMaHVUaRo8YAvVq17Rt1FiPtE8PgaVVkmIx5IHEKEEQBEEQoYAjQhQzu71e73RRLc6KFcrDGBShOoq/G4UXbxl5Ngcqbp8vu+D9lN2/A6p89AZ4LihnxiMonCc8/KC8D3Zz1+23Ql+R21+SbvUVFk63fSAEQRAEQRAmsb3XfOde/ZdLHo8wDb1erXstjanUwqNPvwB5+edMrwdF6JgRQyA62lx2vF5WfvARbNuxm7tUQWHhig+XLhRnXhEEQRCXaJ2QFAMAceyDpONn8+rlOXSU3AOdp/DCNiGK7mFfYeE6yeOpJ5qveaMG0LXT/Y4f1I82bYUNWz42tY66cfdC357dLBuTXhYvWwmf7fuSu1RBYeFIKutEEAQhhgmbSVikhDNjCgBMIKETXOg8hSe2CNH7uvQoGxVVeqcoMx7p26ObLYk9Wpn4+lRhn3kRWKi+W6d2lo2leJ3QGtVu1LTcpCnTIPPUae50EqMEQRB8WickoVVtCwDEqBwmFDetNq9enk6H03noPIUvlgvR9om960RGRqbxkpJATuyJkssz3XXHbaa3l3nyFHy0+WO5WxKut13rFlAxll9zM5C8vDx4c/oc3WIUwwisSEpC8fnxp7vgux9+kv9f/FzUrVUTWjRpIKwhivuN+5B56hRvlj8vXrwYv27Fks9ND9iltE5ISmZvyWo3KDvIYW4hJI39P23z6uUZ4Xq8CSJcYBa2IzruHfi9rkUWN2eh8xTeWCpEO/To2yPC601RE6FjRg6RM+TNgh2HJk6Z9o8uSdhWc8yIwX+LUZXdy83Pg0VLV8K3P/yoOpqyMddC357d4aaq2qyVIt7/YJ0wxjOQ5rL1tT13+snTp+GN6XNkYc3hz4LCwpprly06bHrgLqN1QlJVdoNyG+nMTZRKN0OCcCetE5LGs5dYPaDrdzKdUueg8xTeWJY13yGpzwivx7PU5/NdxeuUFFu+PDz/5KPyb276vI7Ptk93yeIrcBv4/zdmzJEtpWoiFCkTFQ3DBvaB0SOHwl23K1toUTR37dQOHh87yrQIReH78pSpmkUogvMuXPY+dzoez4dHYsIUt+D9VR5J+hBDJvSP2PWMd+kA0Y00C0Uyu4kSBOE+hC2mORhZhjAHnacwxpJe8x169J3l9Xp5wcMyGPM4bGBf073XAznBiY3Mz8uHhctWypZXFJpaQIHpF5no5s5lGfUVK1yveR1qZOXkwKx5i+GkIKaTx959X0K5sjHQrnVLxTlQjGLM6sKlqbxV3FqqdKmdAGA+HsIlMHeN8LpzAXJwfeuEpHgASCTrKEG4ijgDgzGyTImGxXfivTDDYNgSnacwxrQQ7diz33KvWnmm2jWhb5L12eV333EbHC6W5OMHxR7GTuoRo35EMZlGOXHqFLw5fTbk5fHrkarx0aY0qFerpixIlcBpuP7UNWsVp0sAt3bo0Xf52mWLwqWsU7wLxqCVeGYdpSB6giDCGhYy1Z2JwfiA2E58EQ9HzxxhAsOueXTzdurZ70ePJHFFKLrK72/d0hYRCkzgVih/vWIYAH7QPf/+6nW2bFsPe75I1yRC88vGyh8RKEZFYDxpvdr8F0E8Xx169A2XuJlQe+PFm/EWdpMmCCL4iG+oyhhZpkSAiaOtE5K2sLj9SUyMBlpOjCaU0nkKYwwJUcyMZ27eW3kiED/9enbnupKtAC2dw9HdLygmj3U2Uz8InhhFEbpo2ftCEYri83DXp+Dn9o/IH/w3T5B+oyGpql+PbnD3nX974IuH3UqS9BjG9BrcJTcRShZRP3gjXuGOoRBEiYcbyyTAyDJhDcbBt05IOsLi4kX3ZaOhSXSewhjdQhRFqNfrTUM3L28eTJp5YtxDsqvYbtBNPWbkUKEYxaQmFIROs2DpCjnJSJR/9UflO+BYm5Fw/oqYS3/Df59q2FNx/ty8fNnNr0a/Ht3llqO8fC3J45nernvvNiF+aYdqDFAcJTARRPDZvHp5ik7LWRpbhmCxn60TkvYx66cWT48hKyWdp/BGlxCVM+O9XmGN0HJly8LDDwyFiuXt77vup1KFCnJxfJHgQ0H49fc/ODIezIxHEfrZvq+E8/1erTZkNhsIBZGXi+j8mPJw4UrlUBotcaaYFPbwA8nceFLEG+FNxRcL1ZW5EBb8HsqMJxc9QbiCRI3Ws1Q2L/F3HOgWnQYBM+5yOk9himYhiiIUrWg+gKt4Yg8tcE+MHeWoCPVzzx23yy5pEZhRrsWaaAYUoW9Mm60qQtHieaphD1vHgmIUQxfK8CsVXOXxetNCtKxTqIu4UMj4J4iwBytZbF69HIVLLVb7N9B95q8HjMXRqerFP0k2EPNp2F1O5yl80VTQHjOtJUFSElK/dk3o3rmDpeWZjLBizVphjc7oqNLw5NiHZMut1fhFaObpv8szFT++haWi4b+1E+C36rWEW4/KPgU3rn1NcRqOv2KsdrGPHZymvCP0Uvx44fyFxhtXLcu2/KDYhMECx1op/tZe1SbhizdWyiAlCCLkYElJeuL0U5mQJIh/ICzfhJayyFKR70mSJIwlRBGKMYlO4RNUqu/euT3k5ubJSUpKYIzljHcXwSMPJAvjSo0wZVqKsO87itBjbR6A/LLi8lAoQivsWqo4DYW+HhEKrIZr/57dYQG/xuhtERHeJQDQVteKg4uVwhDr2k1Wa83JaoHGs0xQK7Yf0zohqfvm1cspqJ4giFBDrzWUstgJRbhClInQHShSRFZTFDhYRkkkDvWAbTuzcn67tESlCuV1C8bEhA6yIOSJQvz7zHcXybGsVrFuU5pQhJ4rGyu747WI0Bs2vQOe88rtOu822J8fzxEe23Wc8k+Sx3Nf+8Tey9atWGJvvIB1WCVENbeB27x6eRq7mU6wsL89xYkSBBGK6IkNzaHkIYKHohDFBJbIUpGrsGEPb0EsndQ9ob0scMyCyTdbP90Jn32e/g8R6gdjT1s0bQj1BfUxA0HhihZPdEfzxOHBIxmw4L1U6N/LGksujp0HilC0hBaUEgvqaw5/Af/+fDVXhCLt7zNesahdm5aQlZ0NezhjlSQpqV333l9/lLrELpe3lZgVgGCmFzHeVFsnJKWzciVmEqfimTWWIAgiJDCQLEpeH4LLZUIUS/p4vJ5UUWY8itBHHhwq92A3y8EjP8PMuQshL5+fCY5icuHS9+Hj7btkC6wW1zSKUZz39XdmyS0/lfjsi3Q5q9yMuEMOHs6QrY1K/Fa9jmwJlREYjcv+9Clc//lq4XbatYkXZsFrAWu74lhxzEp4PNLEdt17//ZR6pKZpjZkP2az5tOMilA/2CGpdULSMJY5avTEkEWUIIhQQ+9Dk9zyQQBDv9izUu15iecnJVhJXv/Imm/Xvfdwj9ezUSRC0Tr55KMPWSJC0TL3xrRZkJeXh1k9qh/slITCEn9rAQXr2AeSIUrg2sdORTwLoVZ4ou78lWX/FqECMB5UTYSi5dmsYPYzYlA/qBjLDxHweKQZbbv2qG3JxtzLBCtGxtp1mhG0JEQJggg19LoSKYvdQVonJMWwZDJsnjKevTiIPpOC2WjlkkVUtoR6pBkgiAdF8aIm7LSyYvVaudC8XtC6OWPuInjq0dGaYkdxzImdO8ilm3jgtEqx5YXizAh/Vr5TuJT3Qj5U2PkeXHX8O+F8KEL797Iu2RDPH4rRF159s+glQGlsXu/Wtl17tNywctkXlm3YIiyov5lmcb/3VBsz+C2FxbbGszfkqqzsSTrLaHXEasHe0qsGWFXiAizKGcU+Vp8rQ7BrLp6N2z/eQCtDDjuO/t/+sbvqAYwPqID9UDr+xffDFcdfCTdcyzrHGxcgBKDYcddCeoCg8/9b/r44eY5YxRK9Hqnu4WgVZQmsccXuCbzvUrqD1+WkUOo8KJdvkhOTIiOOiiyh9evEyW5dK1jw3gr47AvlrHatNG/SUE5K0srHn+6EVEHf+dJRpWHsg8OgkgExum7jFli38fLrK/u2pvBLnQTFZbwX8uCGjdMgKuukcN0oQPHY2wGGPLw+dRZXjPp88GNBQYHryjqxL/8WE6sYZnXgPGtvZ0ggb169XCq2Ln+haC3rw5tcotoNjh2zWSrrTGPHhls5wChs+8kGLCn+fUxlriMnH7hV2XiTTViu0wPGHhRRysSnfz+M3EwyWI1G3fvg9mtZ5/hk74feKhfs+K+wWRjksH32C3FT32F2XMYHvLRksHXHmNiPFLaeuICXB03XlhPnSSvshSJZoY++Fvz3ssl23Gfh72N1xMCiwpyJgBepuIAXcggwHKQZvfZk17zX650uEqEtmjSyRISi4HnxlbdkV7ioR/3FyCj46/pqwnk+3r6TK6B4+1C3Vk3u+oosrQt1rVONq45/rzhHVM4pqJ76vFCEotVy+OB+tolQYGEWAwTJWpIEt3m93h0hWvBehB03KKM3FaXl9JSHihGJO+aiWaHxJo43mX1Wdq1C6ycT6VsMilAIKP6PY9tid1ctdsxmsZu51taFPOLYOrJxnU5202L7MZ7th5mkuqpsH44YaE3r9mtZz0tGnN5rgYlQvfU2jeA/dv7ztI9ZjHXDrtF9xY6N/6XMzH74K410L7beSUz0qi1r23nSAmtnuiXg2BjJC/Dfy/AcrbDpXma0UcplL4D+ewi7h+8LOH+B4/Z7V/zX3iR23WvG07Zrj2qSBEk8gYYWOcyON8sJjO+cilns4vhOtCAe6DURjrV5EA70nign+/D4+rsfdY1qQO9EuPvO27nTs7Nz5DHqFaM3Va+m+PfIv7Jk1ztaP/2gOL1hw1TwCjLjUYSidfYewVitAo8HnmNeiC6W7/J4Pe/YPhB9mMnYSneZu1RJiOq92SvedNlNbp9OAShbb/TeSBS2XTUgRsnKh4JfYNgSCsEe3kds6nrlfwCNN3t81WDhD0csKjHmB9czib0MaF2n269luwtgG7VCmwW3iS8+RwwIUiuvGa2ojdG5QuUKsBewfRa/UHRn9zK9L3da1muES8+iYi+xel/Gcbktel66PSBJ3EEP6J1kSXkmWYROmyUUoQWloiGj02OyO/vS3yKj4VSjXnC2fHXFZbAUkV7QAlixAt/9jmOc/54+g9lN1atCTMy1ikLumkOfw02LH4fKG6dB9fefh4pb54DnXB5X+GES2NPjxhgKETAKWl0b1OXfKyWQerTt1tNNWfRmhI1drl2jDxul8eh9CMQXf9gG3DiNHKuqZmJe2YPP6pt2ccYzq49lD0xmBZ3lwEN4ktWWZz8BVsMVNu5HPHvQaFm/269luy3UwW7jW5UJUj3CIBiiT+06CUpSJ/s+7bM5B2CSVfcydo6NHiu8p8YXE6BGxxTH7nGaxuLxSJ6OShNQXFkhQtENjyKUV0IJuXBlOTh63yjIi6mg2MP+7L+rK4q2A4f0e0Ojo6PhkVHJEBUVxRWDX3/7A8xfslzXehvUEbfsvOL0YYj8Uyyc8ZiPHTUMypVz+mW0KBZVJEY9kmdY2249hzk6KHuwK/7R6ElTGo8RgbKF3USSmSXS7I0z2YgbmVkqnRBzwI6THuscFyZCnRQNVdnYLXvoB7iBnRAScRrcqeDma5l9b/Wi+f7BXjTcUhXD70kQ7rPBY2IJvG3bfZ54BMSlOmHRjtNrRRSsxyjj2f5aZRGP0ZqJjzGi9ZXEWMumjUyPYvfn+2D+khWQl5vPFX15ZWPhSMdxqh2HrKRMdLQs+ERZ93s+/1Iev1Y6tG2lu/VmIPXr1JTHhGOzgqysolqhco3TLG2e6MSEjvI++Dg/kiTNbNu1R5KtJ8d+7LCImnGtWJVFGcduIrMstETqEjRMzFntZlIjzmzZkSCIUD9+17Hp88VEzxGH3cDjbYp5Dfq1LEBPWE8wXPIiYpjYCbaV1glMhV8FJJg5eQ7jLAiLcls5wDgtoQce3gRRLKUW0KK4YInYxY3xn8fue1B2y4so88thxaloQTQKur5HDukvXBrHr0eMopAsLkZ5oi7wp/19LeUwCLNkZWXDvMXLYfT4Z+Dx5yfCa1Nnyh/8N/4Np+E8PFCYjxs1XBgWIHk8s0O8xqil8aHMomX0YZluV+akRWjeryCKOWDuXEMCmJ2/YD+YTT18Aiw3zrtSgn/stKJ0LRsRGXpeZINxPrQwS/Dy4zbxDA6cp38Q4FkIVmyvGS+PG0s2TVJ7YeUKUTOgeEOLogi541DjXqoitMKO92S3diB+EWe2yxAKWWzxKRKJy1d+CCcytRXQ91taa1S/8dLyIqKjomBAr0To0La1qf1A1m7YDI8/Pwn2fJGu2KUK/4bTcB6clweK0YG9E+XuWRyukjwerDGqnKHlDIbfQKws/8NuFrNMrCIs2t4xERhsQaLbOmfB+bOKGKPHL8ByEyzR40bhYhs6Ex3dfGxm2Z0wpxcra2yaTEidFORzpzXsRQm3vvwI90cWokoCzCi5eXmyeBOJu5ONeskfUTMl7LdefscSuObwXu7Y7rnLfFZ5g7q1oIOgYxEKOLQq6hGjaFVEgcmz2JaLiZG3OfHZx+XtmwUtnWsV6pjywHlxGR5Y2H/cQ8Jw0KtAkj4MYlknt7gfzFihclj9vJCGWVbcUMw/xsDN22gJFjswahk3U5aJ0IdeceNWUQBmExJtwErPkGER6hIPCbAXayPhJG69FySLXnwieKLz6+9+MFQ+aOsnO7h94wtLRcMvdboISzKBv9j7hqkQlc2vs4kxrFbFU6JF8kxWjmwxVAL3Z96S5TDuoeGat4kCEz8ozE+cPH3p72WioqBSRWviYXHdr701E06olMRSAq3WaFHueL+yNRbFKIYLzFu8jLeK2ySPhB0CGpjaiRCFuaLNfOknu63rjl4CrHFmyAlwo+ntNFMcrFk6QctxZWM3E88a2OXGTKYqsBAN3dZxZom2IvbR/xZbvFtUuKP3nAWrw1ROsW2bvd78oDgoXlg98N/+l+Ucdl2YvdbSA7xA/uLo/u+76KXckfPE7glmxXlGwDE0e57Qpe267mwm6M47zxE+n2+PJEn1i09AQWlEiH717Q+Kf0cRerTtQ5AfIxZhWOy9wo7FQhEqWxTbttI9NhED+yTJVk+eqMMSVCj6npnwsK71onC92UQsK48TmSdh3uIVhkSon7Ubt8hi+bpyyobNBnXiIC83D5at+oC3ivptu/ZYumHlMvWG+u7AEtePBfGQoWINVTte4w0KR383lcu6cDD3erJBa6W/64sWUWekK0oKa3l52foDWmcmG4jTmqBz/sDON0bICTj+lz20A6xCWvbDle0/FVC6lt2W2FGcFF4HnoDrLd7kvQivoUD3Vxq7HnOKd59jSU5GQlm4HbNYkl2MilveqfNktINaGrvnpBYXjQFdzYzkEvjvhdxuR8W2ZVd8qP/cmDUUxHOFKAB8iIKi+IQDhzPk0kt6O/ugYFMis1FvuTyTCBSfN2x4W3bL84IDMIZx5JB+lllDAxk3erjQwoh/RwvhwD49LN+2HlCEvjp1JuQJSmJpBeNFRfvTslkjOB3lYwUAACAASURBVHHyJOzay0nakqQe93XpcXTjqmWPO7P3wcWipJxQsYZyHw7sAWJECAnbyLGH7oTWCUkpBrNW4zQKUT3rVW09yc6n/DBiDwStcZuTDcbGGS2xkspaX3KvPya0UzVe66EsRO3GjDAQtiEudr1NNlFhoHugEGXrVfx+4niYGNX7nZzAu8adbNkrwqA1NIftm9p5kluYsmOn93uLVtFgtAf2tyX+x3kLCMUy4jnhWtQ94POl8roqvbt4GRw8pL1l6RlBRvafle8ULnvt4c+h6geTZRHKo1KFCvAoZnVXjNU8Jj2guB05tL+wrNPuz9Nh2UquhdB2du/dVyRCc/O4AbbnrywLR9uOgh8Gvi1/8N/5ZWMV5/3y2+8vL9xa7DOgV1KRdZyzPUmCCW26JIVDjVEhFonQNJEQs5AcZtlohb3sWT/7VjossRkqDwkjN+1ErfvOBGkrA0JH6w1Sz0Ob+yBVgs1bTcPYM7RaOwJhDwMjblJ8sCRqfahtXr18mMr1kmFXP+9i2H0ta8UpMZsmEjfFwe/K5tXL9RyPQGJ0WtL0nu8MPftiEUbOk977eg67HvWcpxR23eoVlU7GrPrv04ov3vi3zauX1zIR/qB4rXk2rFx2BHw+biDgO3MWQCbHymkV5X78RHbHi0ARihZLu0SoX3xdV7asnGwUFVWaK7wwbGHXZ1+oCjirP7hNtMjKIpRDfrmKkNFpPJy9vsalGfDfJxv3UVwAGw0cOKz+sjGoTw/hsfdI0szWCYnWxku4BNZdY4sFNwQUHokO7JUshFD0Bd5M2E1kWDFXHA/uPOxmotf6kqhXtDDBlGhT7UbNLjgjD1I2dtFDx3/DN2LpMGKJTmHnXi8TBIkkTrxQ2XotuxTdoRqg7cWBh51xwcGwROvCQLy4X4TqFmNsGb3PSa0tgc2eR/9+ablPGxHUwBtjUda8Dx5Ao6XSDJgQ8+rb2tzAl8UaBoi3K49/p6jLUID++7OV/PR5nw/q146TRahcUsguoRdA5dhYGNRb7H7HOqlfoTXRIdA6rdbt6bcadeU4XKWSWGgRRZFqFCw19ehDI+TOVDw8kmdlq86J5ssAuIiAHtdm42/MCA89YOJLKxXXa4rKwy5VxQKoV5DrsigGwiyjegSs6g1bbyyV0TI3AUJaiQlGHmQGXwLSDYpQkatWl9XOIE5cy27DlPWWnWe7vAhgILvdzXWS/eiNF59s8hyl63zZiHGoW9owrfslCuFQgS9EN65all1QUNCVJwRzc3Ph1bdnFIlRFUFXsUL5v5cNIPbTRXDFL4cu/cF7Pg+qbHgbrj20Vzjsls0ay9Y4QV1LW7j3rjtUi8y/u2QZHD/JT6qygtz8PJg2ez7sUSmsjyIUrZ5KIvTvU+lTPsUaxymL0dEjIDoqWnE9WNbJ6/GsdqCskyOZvSwreYsFwfKG36ANoNXiyhN36SrW0Ko6b4pWhCIEW0gYfggwEVT8oSOM/1PBiFXelBWejTXwHKQ7ZNm39Vp2KVaEOui1qOoRYaFSaUAPer7f6VaEVrF16Dk2dgvRVANhNkauVcXr51JB+82rV2wp9PmG85bGBJl3+aV8LoECTglZeK5/G6p9MFkWoLcsfgyuOH1IuC4Ugj26dFLdpl00rFsb6iv0kL/UnjQ3H159a4ZsNbYDXO8rb86Ar7/9XlhzNbNxH8hs1Ic7HYk8mw1RWeZFM4ZIPJA8QDQLml0/tVmM2lqfj7niV1jUc9dJEZqitVMTm6/4vKlsrCKrrd4boiE3YzGCndil1TWmCHvoTGDHt5ZRERqQgasHxaxrAySyfUCrfi0HLPtOXMtuxPR9gr386FmPnd14XH38A6oPaMVKL4CedcXb3IBA932afe8ssb7/o7PSplXLZ/l8/CQEFETzVNzD8c0bQ5Qg2ScqK1NRgAYKp6gozIwfIAvBYIPWWKwcUFzY+UEr8Ss2iVEU/qL43ILS0XCySR/4rXpd4XqwLmvlNP41f0t1fU2Sbq5eDQb1FYYu3O4DnzjoNzhocduOZz27rXgDdVKEgoGbpN/KlR4QoK724NBjkUuzaN/1WGHsONami3+zGMdEk8fDiDXUkjhOvC7YPjjVDcyJa9mNNVOtun5d0bXNou+/nedJz33e0sQrti49L4lqYzXqudP80qeAbm+VkqC+rMUnluEp9PmW8axrmDCT9skO7kbQhY5WTJEFj2e1A1aeCd2/PMuqEfYfPnLpY4QeXTvLBd55oFicNnueZeNFdu39Ar7m1GQFJkKP3j9aVYRG5ZyEKuvf4lpDi3en0tIbH39QnMc3a8ydDgBtWyckvmds720jjvdWibUTWyckHbHICgrsgVLLQRFqJLZsAksEqaVFYDC3vJ6bnembNovR1SMCVS0wBmMGsfj3viC3RdT7chSMsi9WYPu1zHBd5yOLrNcQIrGZWrHzPOkRuXaIez3rVBurUSFqJvTJkoSlCKW5Nq1a3rNNl6TKEkiKXXOwfFGZMtHQQMFtDcylfeDQYdi9V9+9BMXeY6NHWFIjFIXclm07FC2KKL5Q6Gq1uOJ4cFzPTpoC2dnKx/3goQx4d/FSGNTnn7XdjbZL/WD9Ju6y+WUrwvH4ZLhwpdj7LddlXf+mHBbBG0V88yaGx5jUtaNsCcZjrYQkST1bdU78ZMuaFW4q3r6ldUISWksyAuIdjRYy5qFaq9EGdN8k2fj0jFGXC8+s9YwVVp+l80Gk9aaaYeCc4w30COtG40TG+CXYtarXMuQKq5gBnLiW3Ygr2lyWMPTc0+yIVcd1as3YtyOEIsfkfdoSQ8tlFlE/EkgdAIBrklv2/gfCrj4oyFCoaAXFoVYRKrLUHc88Cc9OfF12a2MhdqV5MNsdp09+a7pc+1SLBRAttQ8OHQBR0aW582DR9y2ffGq6Zz92eMriCF4UoUfbjVYVoZgEVm3NRFmE8kCLptmuT9iRqlKsoKyTR5rVqnOiG3r3+vGLCR9zwU+yUIT6Cxw7kR1fHCcsr47ctNEKysplaS0Kb2S7Ro9XDCsyvc/GTiZK6N1WRohliwcSKkXyrSZYVkw923Vz/3xdMO+GnjJuln+fdK6zqg0eGbMvq5Y857hCFDPpL1y80KnQV/iHkug6m5cLr7w5nSv28AeFzqRnn5DduDwBd/edt8O40SPggaEDZLGnRRTywPair7w9XXPby4OHj8Czk1+XhZ8WsEf8Yw+NFM6J1mJuFyKN8Op6YkY8itCCSLFYL/fDNojdsUg4DyYd9ehqTSIYvkBUiq2g2BTBVxR38Wq4lXVSIJ3FgzpqKQvAiYe3Houckdih7kyAGi2XpScm1exDJY5Z17c4JEj1WkNDVYRCCRaiVqJHsOgRom6MqzWKrfczm9Zt9fE39V0zGHqmzTXvZ+uHK4+06pzYEsC3VZKkq4tPR7fs3EXLZSHC60ZUrlzMpcQWFHy5+X/XI7WyBzuKv3kasvqLg8lGGN/57IRHhDUy/aAYxZaYom3htMqxFeR5jXA2N9cv4P7BH5XvgouRUZcXPg0g9tPFcO2hz4S2WOwv37NbZ0NjU0K2FicPlC3RSklbkgRXYx8AvJa2rFlhTqW7E2HbSiewMLZMEb3WAx3rjQvoxWx2/ZrPAWtXaEU8cDzLaE1jMZl2ucN11w61aRy2Y/e17GKsPGfBTvALBfSIOjuPUbqO73ecxaLYFd81rkXUDwqHwkLfEN50tIhi1riWgvcozFB8+j9W8e6iZYrCkGehK/5B9/ya9Zs0j6Zh3VqQ1KWjcJ2T33pHLnnlFEXlsd6SRagIrGowuG9Py3v1lysbA+PHjOSuF8Uo9gFwoMaok1zq+hLkcbjNGgoigccsn5NYctg+FiNl9oZgpHC5ledN7i+P+8R6SluGwZcAcsuHHlaG8+h5cSmpwl/PS6idoVZ61m2pa94t4TuqQhRJ+yA1tbDQJxcGVhJdxzMzYenKNbYPtjhFFtmlcrIMxyWsmbRtO3SVYGrVvIlsWeSBwnxqyjxDZZ14Yo5Xd9UvQtXqsmIpqp5drbOEFgeTzZQsrQGVEW6/WFDwqW0DcI4clozUyiXWGyfiUfUKUcwyR0thVSY8xzM3to/FflohPv1kGClcbqCotBZwn2YxQWqVy17vsc8JYasiJdmYxEBtzFAO4zCDnR2l9BCs0AjX3CM0CVEosoymFBb6lvKmY1knjI90iqJi79Nht0o85q/3toP9fV+FHwZPk3//VqMed96vvtHXshMtizfX4NfgxIQjHKNeMXoLZ52Rf2XJrnc5C56Ju9JZJ6HGsmeg9JlMbmksrMv64NCB0LCe/XVZUZzjceHVXZUk6fZWnV1X1kkP/jIxbqoE4EYrUgzrSnWECc9JdmV9mmydqreXvVaqBsSQmn146F0+lK2KFB9qHj0W+YwSHAqhB7dYREXotZaGnhCFIjHay+cDrhjdsu1TVWFoBejyLkqUEicZnWzSD/5Xs+2ltpf4GwvAny1f47J5UTDtP3hY9+gwNrJihQpcEXg88xQsTdVnLa5UMRbKxsQoru+ag5/JwhMtoJgVX231RPAIMuMxfhNd5vfebV1dVjVQjDYSi96e8Z26T3dsQNZwyQ3vwtqMbrSIOkGG2YYB7CHcysZjiOJ7HwtFcCrjOJStim4ee6hkjOsRonpjmi2PE7cBrecpFBOvRGMO2UQyXUIU8Xo8D/p8/LJO6CrHDkx2geWZJr05XRZ4PPGHWeWHOz8OOdXrKE4/e30NxYL6Zzglk0SgG338wyO5yVrAEqnmLuTqd0VEQg6FJ7rh1Vp2Yib7qy88BZUr8ssr2QUmqIn2QZKkEfGdurmprBMPv9hxixs+WLjtIZxqVcMAtg47xSiwUASjJZ9KTKKSy3H9g551htMjFsNRiGo9T6FYisrKMbsmJEO3EMWyTl6Pp0lhoe8HnhCcs3CZqrXSCOj+f3biFGFiVH65WPi53RjIL+uc+JLF6Bh1MYrj10qndm1kIWkUDBlAgWxlUtL+Q0fkj1YwXlRc1kmaFd+puxWtNO3AXxO0WgjXYwxHMnS0cNQME6PVbL45+931WgtYE4RmWNMDPddWuoOd3wiCi24hCkyMSpI0ACsKKU3HmMjJb0znFmU3Aoq4uYvE5ZlQhB7VIEKjsjMV/35dWeMvG2h1HDVskDCTfs7CpbBThxgdNWwgREeV1pz97/80qBsnzGDXCp7HNR9thGdefg0GjnwEJr/xjvzBfz/61Avw3vur5YoDPFCYT3j4AbiunDBRfo4La4z6rW3BzoZ3E8G2hGQEvBjYUiKJ9VNvxeKA7bSOopt+lo75w6aIOGEPLOxDbwMIN8W5EyUYQ0IUWFknAGjJm44iZuqseZrKOqnx5TffyyJOJL5yqteVLaEXI6PlGpq8zzWH9sKVP3+juI5KlcxZUTHJaHC/nsJ5lqaulsMLtIAC7tUXnxYmRBWnZ/fOMKRfL1P7gaBgRrH5wUebFQv+YxjDlo93wH9efg02b+MnwqMYfWj4IJEovtrn8211SVmnnABrGwXw/5NgCdF0VqHAsTJZbDvVLC7vVJxkHWI0nIqIE/YwS28WuBMJlxYk6rkFO/ejxH+/DQtRYGLU5yscxhOHx05kwtsz55oaIApaFG8isu5oDieb9lXtOBS7fRHEbl/InV7zLvMJPRgX2apFY+703Lx82aoosiQGggIOrYoocEUF8hvWqyXHg7Zu3sTM8GXeS10Ncxe+x7L9RbLed+n8zFnAT4RH9zzug2A9V18suPhp4/s6BtPyk8qSkUK1P3c4kcFEIJ6PWsGoUMCsoxOYILXLQprMiuoToYHd9yfdL3toCW2dkLSCNYTQg1PenmDc08mD4BAsHMQ0ws5KWkj74P2U+E7dr+Vd2BhTiAlMWNLHCGgNFSURnWzaD36rUVe4Zu+FPFmEXnX0G+48KORUXMia6dUtAXJz82DXZ/sUuyCheEOBPuGRBzW7z1Hg4gcFbHERe8tN1S0ZN47rvRWr5bqsesFl8Ph1bn+f4pIootFSO2chV7DeXqpU5FrcVUt2Rh9B74xEyOIzlRWmd03cGrOMT26dkJTCspGTLbYOY33VNIvjkOlBbA92W650JaUxa6NeSyg4ZQ0NIlqPR4aO73K8jeJdz3l3W0yvJfdCUxZRP2kfpL4CwK8xunPPF/CBjs5F/1z2c4W/+qCgVBQcbzUMcmrUFdrsMMP8hnVvwFVHv+bOFR1dGnp1TzA0Ph4ouirFludOx2SuSVOm6V4vij0UnoEfK0ARiuMxIkL94DkWWXqxjmnndspCFYoy6Ru27Nh1iSU7pJ1hJEKDht/yWYu53ie4NXmCWUixdFc1VnvUSuE4y+LSTiXe1ReiVNVSVYFZQcezrmRGzrXu5g9hip7wKztDk/SsOywbPlgiRKHIMtoLwLeeJ/bWrNuoK2vcz4FDhxXWB3C6XiL8ccNdwmWxvFG1VS9DdJZychJcSqjRbpnUA1o8RZnvKEZFLm2nkEtiTZmmu9KBUjjG6nUbhMtgNYCGdWsLMumhV8uOXR9zaNeHhbllwEqsvAGmsJJYQRGf7EFuuOAxhm+wpKZaFiV8VNVZ+1ENEqKhywqeGGUdytACms0aRBjBSCtcM4TLtVjVKjd0IGydoVASy0ouewGwTIgi586d7wPArzGKblmlxBcj/FlFXYTe+NEUKPVXFncevwi1q84mitsh/XuplHX6QhbpweL4iUyYOGWaLEZ54vBiZBScaNIPvhs8Xf5ktHsYzl2pHMag5WUDj8ktNYSW3MnN23cZavMhSSURqguzYjGHxVqW3bx6+bAgl8RC0RdnNpECBTTuC4sjNRtbLBKieo9VjB0PTcKR5JsYVuLrCOvK5f+3vzWumReWnJJiDdV4nvTe0+zoDBfqNYKNfB8uM2pYKkR3bPww59y58415ZZ2QSW9Ms0SM+rslKXHtwb1QffVL4DmXyxVX2A3p+cfH2V7sHdc/YcwDcptN3liwRNIOxRAEe8FtYmmmPEEL0vxyleDn9mP/0RoVGwIcbzWCu4yWDlWjhw+ByhUrcqd7PNJrzdt3sfOmP8HGdTuN22MC0/3lsOzqSsWsnFqz0P0Pc0tq2GIcKVZaMNkutKrFIseOh6YTuP1admp8Vdk5jLfQYmZp7V2Xo+U86T0WbhCiVp4/17ysWipEgYnRgoLCFj6f7w8l0XU2NxfemjkX8nK1lXXiiTdMPFIqpv+v9I8gdvt8v5tXkUqxsfD4Iw/CddeVs3r3FalcqaJqDCpmqRtpMWqUTR9vl7cpokiEPqxYlxX/htM1oRCt4a8xGh0VzeuQdbUkSW9xVm/2rTA9zMozOeH+MnrM05kb3u7j3Z1loQstRszt6b8BJ1sZm8kqLpjp0MR7KBk5drY2ikCLq5nwBgEUVmAPE4LkhXDzC5He71V3Kz0NbF16v6dWWkSt2Bf3JCsVZ9u6VemSJHXlTT+TlQUT35imSYyiaFQidvsCiMr+ux5nUWb8Qvi/L9cJ14cJMy889SiUKWN9TKiIxvXrwGCV+p4o0NFVrglxVSXhZ/b8JXJ2PK8zFn5yatSTXfCiuqwXS0UpLw+XhfQqgqELj48Vxuc2bNY+QelGZvatsKRYBazE6DFzygrjF6BqQibweoqxODbT36Ep0eDiVgrReJvd85NYeINbO6PZRSgK5RQLEzJD5d5ph2seLH7BM7IutxlQ3CtEoSh5Ka2w0Me9yR/PzITZC9UTpBs1qKP4d+/5PKi+6kWovvpluPGjN+CmpU9CzKE9wnW1atEEhvbvrTzRhLDT+mlcrw5L1FEWf1jy6eUp0yA3N1d9nQaZvWCJanenrDtaQGbT/sLwBzz+V5w6ZHwgjMqxsTB0AF+gSyCNMb0RwgqM3LQdsTwzK6f/waN2Yywu9sZbnLEOzPJk5Hjxxm70GNrSSpQdb/9DlGJRrcXqGMB0i8OQ3BBXaQns3qRXWE+y4gWPrUNvwlmOCz15ltw7bROiUGQZne3zwXSe8Er/+ruirHGB4GpcrzaLr1ReR+kzJ6DMqYPgOZcntPBhQfje3bpYLuz0gkK45t13cpfCTlSyGM3LtXzbWkToyab94XR98YsaitDKW2Zwp98gKLyvRM277oQ2LZrKUxSy6NvpWlnJxIm4NSMPSKesJ4GCS80SUnx6jE2CzUjyEu8BZ1ScJGspB6QHJtoDY3GtFholOkYUGzhYmFDkD4sJqhXT6hc9jWjdppFwBT3tea1ch9WhFVZ8dy3xENgqRJGP1658wOfzcYMRMWFm52f8RJ0y0WUggVMkXQtyLOIjD0LjeuKi93pAkfjTocOGxeLQ/uIuSZjMNXsBtyyrIXC8eKy5mfGlouBYq+GQfVM91bqsVda9DmVOHlRcD7YjxXOml9Ytm3Ljem1OWnIjeh8cth8f9jDT+zZu+7iYa7i4u503L288460WbFbCrCBGLSFW1yhdYXPdQ7d/1534rqVYIEZdIUIZwTinWrdpRNzF60iMvAy2rJH7TTArjVyGlaE/tgtRKLKMoj/8e9702QveE4rRNi2ayV2F9IIiFJOSbhWXCtIECjmM4ew/YgwMf/hxmPj6VPn3sEfGw5sz5sjTtYJCDcdVruy1XGGY/vW3kLJgselx+1m9ll/fs6B0tJwZ/0eVu4XriMrKhOorX4SoM/w4Vr9lUy/XlS0LN3B6/fvAZ/UbdShkmbsRI2WEbHEPw983Qj0PBNF5X2Fx1rrVLmujD6GqrByQ6Wve4APUrdeyq2Fi1GhcZ5qLRCi4/OXC6Pcq2YgYZcsYjUu3vP20STFpdNnL7gmOCFHkwoULTURidPGKVXD85EneZNmljcXQecJNyTL3+kvPwA2CEkFa+DUrC16eMlUWnigOi1vtMOHqy2++k6fjfGc1WkmviC4Do0cMgegy/Bqj2JFq08efmBo/sH3gZeTnX1cRjnR9CvLLiY8TitAb170OkX/y67JiyEHcPeL6riLsaCrAIewsrA7VjTRy0x5vR+1FJqxWKIhLow/fGGY9tCL+K8ZgIoLo+JoRdHFMjBrat4AMeUsTu0Tbc2I7BnHMco7NHgw0TMASaXaKUCtjn+1E03li3gYzYnSfxm5Y8Sa/Q2k2nVMz58bQfV1pPxwToljWSZKgM6+sk5yo8/pUoRjt0r6tLC4b1edbR7Fu59D+veCJR0bJYs8MxzIz4amXXtFcVgnne+TJ5+TltIAiGccpYsmK1bBjz15T+3H8hPIx9VtCz1/5dxkrpfjaaw/sgWorXyiqy8r5wQ5SmHTEm67px6c8lwJkZbkc22/2rDyRXvwFui3LNmXCltfe0Kxg22eBcB5v0OouetCYtYb4901zcpbGVpJuezjajsMxjxM0XtMZrEKF3fWRjZzvoIS96DhPZr5bcQENCDCRKZmJznj2b/zbEZzHpAHEcmuoBVj2PY1wctRbP1x5pHn7Li0wdBQAri4+HcUolhZ6/JFRXOvYv8qVg+T+feSP3x1+5kyWXBP0urIx8nQrQPG3OHWV5nqnfnD+t2bMgReeekwWwhwhdQkUzthpCBOJeKQsWAKVK8UaLr5/7ESm4jjyylaUyzOJhvh/6evg/75cK96H2Fh4Yhz/nGkBk7R0hDeYfvjhjSLI3X2sxqmHY4qBt/oY5vpOZfUMDcU7sgfLeJXkIrNZpTFMsE1gZW90XWtMuBkNR+AKDhwHO35mBH0My9Qdz9aF13+Gv8UqO75x7AETx7aldl3Z8VLo1LWcZlAkxTkVr8fOeyuBkMlhLnzd16qDyO0xTWR823qeMAwCBaPJ666qXVUqWLa8Fst4ugGxa+ZaDq0Y0UCwxigAjONNP3biJEycMpW7fKCl7JYa1eRPo/p15N/XlStrziLHfj7ds1cWfyiMjSz/a1a2HJOpJkL9YI3RhHbihKyXX39bbsPpJBU/ma8qQrEk1YtPP2barZ7+zXfcaQUXC+x42IVb2Rmn9sdMW1QUNmg5mKXHQhrQY/uIhpu96FrRcx1NYmMdr8VVzCwgW0z0ANcyPqusIv76qSuY6PaxFpLZTPD449js6E6jBbd/Nx2NMUeBybLpJwQc7zT2/2p2dixTwKiYDIZVVM95sqrOqh1oHZvTLyJGzqni9eOoRdQPlnVq1q6zDyRpttL0oycyZQshWgqdBi2yOwSJU1rBzkVtWjaTxbEWEjq0hTNZ2dxt5+blQ8q8JYYsj7wOUqX+zFb8OzYHqLxpOlxx+iB/pT4ftG7ZDPokdtE1FiXQGrp42cqiOIDL+R7DOkxv5HLcnrCkF0ce3mg9a52QZNRC4SeZxVflMPGl9EZeNaDNoR64b/fMupSh41j5LYiT2HJpCjfSqha1YcxRs9BjaITO8TuBHZZBtwtR28Yn8tSwovTBFkxmuoaZeYk1gp7zlGIipMZOcmw+bvFGrikT4UuKQtRxi6ifTz5aMwdDIHnT0SqJ3X+cIjcvT5MI/a1GffhpwJvw/dBZcLDny/L/eYisfEoMHdAbbsEMf04xVGwC8NJrb8tj1YNc11NhfZF//gqxn8z/R3mmyL+yoMqHr8EVpw7wi7L6fHLymBUiNCs7B156/W1RKSylNp9WWEjDLWHJyYe3VQ/DGHYjnKTwSTaSoa3B/WfUqliVjan4WJMtOvZaHzZustxk2FRgu8QK0RDAqBDtHoR6oprPE7Mou9Eq6qS1Ww+WPj+DJkSRT9at7iMSoxs//kS1ALsVoLBDgScnBQnE1+l6iXCi2YBLbS/PXVlO/v9f5Wsozv/Tfv2dh8aMHAKVBLGgJzJPFlkPdYC97suVVbbMxhzcDTcvfQJuXPea/Ln5vcchOusEd+XRclvOh6BxA/N1WXFfnnx+sqit6YmCgkJ9O6udcCsp4mQ2b5rbatoxtIg5t8YFaxKiLFbMLcl6WkS9q69lg9gpRF39guyPJzaI0+1gdZ0nZnF2UyJsuoWtWXkY/a4ZvU4Vj29QhSgUxf+NAp/ve574S5m3SHPWuhFQBKEYT9trugAAFUZJREFUOiGIvywoVQYymw2EM3e2VJz+200NFf+u13IJrITRwyOHyoKPB9ZcxXJXeujSoS137lJ/noErTx2UPyJwTE+OHQW33mS+LisW13/x1bfhbG4uV/sXFBQ+zHHLW/GGWDVIHT9sw+GyN8Nc1nc6Q0tAv0tF9GSdlkW7M6O1YpvLMExKOIVTMqQVOFL+KwAjIsuqrlZW4MhYDD4HjQpRxWdG0IUoCo2CgkKsMco1w705fY5QKBoFRehLr0+FrGzlWElgIjSjwzjIuYnvghf1ZDcCxpWi4BOJ0U1pn8CO3drLOqEFs1JsrLANquiDy74x8VnZumoWFKEp8xarCfUlOzZ+qGgNNflWHki4uecd2x8mnNzkytIjzlzl3tY7Hiamg70PKTb3vXb1d9NGoex2azCYENhxTncw03ue2LPFDS96Eww854w+F3V91wKqaxjBnUIU/hajnQHgD6XpGD/40mtT5aQWy7a5ey889eIrkMfEkJIIyy1XCY50GAd55SoK215e9fNXHBFnvIk9Cr7k/r254hBJmb8E0r/6VvM6Hx45RO42pZdbbqoOTz76kCUF53HMKEJVWLJ9/Zo+KvNYYY1z68PO6L45akVibiOnExCUSNFT49QlQs5PopEYMFYvMlhuxBwdD2s3X8tmLJZ2jS8UvDRmXkCMlDhy9Dy54L6WYtAl79R3zczLhLuSlYqzY+OHXxYUFDbnCS904WIcpxViFOtqoiAqLuwCybuuEvzcYaxqx6GYA3sg5uAexWm33mzOhR13712QPKC3cJ5Z8xfL+6MFzJ5/+enxuuqRtmnZFJ4cZ16E4nl7c8ZsVSuuz+fTIkLBooewW60PoVQiJZiCCNi2dVswgizk/Awzad1vZUHdVCMM0yGeQ+la1oNdL7Gh4KUxc805fV6Ndv8ZFiQxmsK27SR6X34Mn0NeRQjXCFFgYrSwsHAobzoKrjfeMX9tYBkkEeiGP9z1adktL+K679Kg4ifzuHOYaXfpB13qKAZ5oMBDgY5F/bWAYvSlZ8bLGfrlysbwW6TeVA2eGDsK+iR1Nb0POMYXX30L9n35rbAtK577Tzd8oEWEQphnzhu90Tu+P0yQtAqSqEs32VM7WOMGJuZM3czYfic6HKs7TGeHrZC5lnWiZXyGLHl2tMS1GKPfGf/16iSGj2UQxKhZEWr0vOgVlkaFKPde4CohCkViFMs6vcSb/tPBw7I10/D6d+8VFobHhKTM5gOF60ALauy2eXD9ruVCIXeDBfGUCIrBRvVqc7eFhfenvDNbV3JUkwZ14c1Jz8miNHlgHzmZCT9o/Ux5+xV4atxouPXmGqbHjsf6iecnqVltse3rUHbutWKFJSjGjTd9E7F3QdmfgILbTt6000yK0EAR7WT7PDy3tcyKUD/MolrNIUE9Qe+4XX4tm20HaxfhKERTWPF9I9+1oJ0nJgydiBmdYIEl1PB3TeuMLObWaFgK98XMdUIU2b5+zVPoouUJr093fSYn6xhh31d8qxyWYjpVP0mYtOM5nws3bJ4OMQd2c7ceXSYK+lpgSQykb89uQpc6Cr4pBqzFKJZRlHbpeL/8QfFpRSyof0xoCf1VbK1FEdpcpwgFC8WDW12ARvcvaPvDbqROWOjwpm1KhPphIjqxWNcau0hhItRS0RggqO2Ke81hsaxG1+/Wa9nMedDyMDa6flfHibLrTeu++V8Y9YRzFMfu8ySEXfd2hcHgOltZUaaJvfQZGaMesW6mBFdoCVGEuWh38qYvWr5Szr7Wy48HlUsU/bdWR8i5qYFwbd4LuVD1w9fg6p+/Es7XN6mbZdZQPygOn3pstFCM7j9wGGa9u8jS7RoFLc+PPzsRzp7NFbnjv2Mi9Eu9m2E3NSusSm4tExOSpV+YxaMaE0VWCzu/VcVywcXWaZdV14qHsRAmqCewB6aV144ZS5YfV17L7FwYHZvqA5/FwxkRBqGQOa92Pfiv+VZqHcPUsPs8aRxD2ubVy6tZ+MKaw16o8btl9fdVNzqqCxh9XqaL7iGuFaJQlLjSEQC47YlQdOntv56Xq5zs9NvNYhEalX0Cqqe+AFFnBMXey0TBmJFDZQujHaAYHTaoj5z5zrUW794LKz9cb8v2tbJq7XqYqS6I8bw2NSJCA7Ai4cSNXSv8hcv13qBSHXYzKxIgiqpZcI785Y2qMSFnW3IOrptZdf3jNrOtwHGbfhhrhT0wWwWEHBi5vosfc1PfEZdfy0bOc4aOWEe9wsXulo5WwTs3KVYJ0GLYfZ40wV5Yq7Ean0bua+lsWVteqNnx13vc03TcV428XKWp1USVzJQYcoLG93WMkSTpKABcDXB5SaQrypRRtRQG0nvIg4p//3HQ29x6oNhz/YaN74D3HLcNJZQpEw1PPTrackuoEhhviS5vjA3lMWxgH2jSsJ7tYynOrHmL5dAJET7wLd65cW1fxwdHBA32xh3P3EBV2e/iLki/y8/vYkqz2o2tFzbu7gG975XGnVHsE/RxB9I6Ial7sb79gfvgf2i55pgToUHrhCQsUD+LXUNpLNnGlS/2dqFwXwuMoQy8L6TrFHwlCtcLUSgSozUBYJtfjBbnX1iW6JkJshhUgydEM5sPgpybLy9aj+WZKm57V7jWypVi4ZEHkuVxOAWKvVkq9TiffHQ03GZBwpEW5ISpabPgx4OqbU2n7dy4dpRjB4ogCIIgCNfiate8H+a+HcSbjskwL74mthD64WWCl9+9DKKy/pnZ/e99azWJ0KcfHe2oCEXQ2tmnR1e0LnJ/UBgeO84PJbAKPO5ooVUVoT4YQiKUIAiCIAg/IWER9dP4vo5DAGA2bzqKM3RJi9iQtg0WLXufO8fZCjfLv0v9mQWRf54RrqtJ/bowfHA/M7tkmplzF8Kne/hF4rFd6MT/PC6HMNgBtkl94ZW34GweP2wBM+MlkLrs2Pjh1qAeLIIgCIIgXEVIWET9YIkfH/im8SyA23ftUc0ax0QiUXmiK04dkD9qIrRrh/uDLkIRHEPcPXdzp5/JyoZFS/nC2wzpX32jSYQWFBQ0JxFKEARBEERxQkqIIsy1yw2O3L77MzlznAdaBrt2amdqDMMG9jW9DisZPqgPVI6N5TbD/19WtuXbxGP8+rQUufWqoBH/dxJIVfZsWW8mM54gCIIgiDAlpFzzgTRq0+FbkOBO3vSxDyRD3L18S+FlLm0NhwHLJo0dNdySBKCjx0/Ahi3b4McDhyArQCiWK1dWXn/TRvV1bQcF4UOPPa3Yi/+Wm2vAM4+NMT1mP2hhxRAHFXZIktRpx8YPS1QWJUEQBEEQ2glZIYplnXw+33YAZTGK7venx48RllN6/4OPYJXGmptYHmr44L5QpXIl44NmgnHm3EWQ/vW3qvNir3rcptb4ThS3L7zy5mVitG+PrtC2VQvDY/aDSUkLl72vWp4JLdY7N1F5JoIgCIIgxISsEEXqt7q/ptfj5ZZ1QjE66bkn5IQdHmiR3LD5Y64wRAtl04b1oG2r5qYTflAoogjVU4Qftz/2wWTNAhi3sXDZSth/4JBswUUB2q2j+TAC7GP//CtvwLHjwp7xWOd13O4tH71ueoMEQRAEQYQ9IS1EQYMYRYvo04+NUa0xipbKYydOwo/7i1qAYjmmGyrFKgtAA4fs6IkTspDjdXYSUblSRXhm/Bi4ItqezHc1jmVmwoy5C1VFqCRJQ3ZuWjs3KIMkCIIgCCLkCHkhCkXxooN9Pt8c3vQbKleESf95wtlBBYDZ/AuWpRoSoX7ua9Uc+vfobs8ABaAIfX7yG2o1Wv8o9BU2p6QkgiAIgiD0EHJZ80qgFQ5dwrzpaMmb+e7CoIwNRSj2XTcjQpGNW7bBr2fEJaWs4B/lsHZ/Bs9NegPOns3l9rb3+XzHSYQSBEEQBGGEsLCI+mnUpsMin8/HrWjfrVM76GpBvKRWZsxdANtVEnv+qFoT/lu7E+SVqyQX0a+waylcnaGs6fr17GZJ0pEWUPguWJqqNud3Xq+3KWXGEwRBEARhhLCwiPrBTG2fz7eWZ71LXbNOtvKJ2mJa8YMF3l+bOlNVhObc0hCO3vegLEKR81eVk///V+zNivP/uF+1j7sloAVXgwhdTCKUIAiCIAgzRITb0fN6vf0LCgq4ZZ1mzFkA/ypXlttz3ixydvmkN+DYCXFiz4kWQyDnlgaK087c1RquPHngsr+rdDAyDZZ9QivuF19+o7aqxbu3fETlmQiCIAiCMEVYWUShqA1oDlrqsA06b57Xps7SVUJJKyg+x//nJaEILShdhitCMUoCPwWlyijbZW2MokAR+tykKfD5l1+r2YQHkwglCIIgCMIKwk6IAhOjPvAl+MD3h5KUOpt7Fp6bOEWxC5FRjh0/IQu5X89kcSXcxVLRcKTTeMi+ucEl0Rn48XP1z87m/Rw/kQmP/eclucSUgD98Pui6Z8v6dx0dHEEQBEEQYUtYClEEs7g9kieBNx3d3CgcrRCjKD6fwxJHefwSR3nXVYZDSc9figflEZ11AmL271ScWqUyv0uUUX46cEjOjFfJyP8DAJp/lrZ+leUDIAiCIAiixBK2QhTZtXndxwAwmDcdLYDT5843vZ3pcxeqitCMzuPh/JXlhOuJObALaqx4BrznlWNBa9e8x/RYA8FkqmcnTYG/cnMVLbTs821hoe8eKs9EEARBEITVhLUQhSLLKLqSX+BNx8ScmXON1xjFFqHYjYkn5LJvaggHuz8HFyPLyA2ZeJ9y326GilvncNdzXdmycJuFCVbYZ3/6nAVqs30bGRHRbO/WDT9btmGCIAiCIAhG2GXNK7Fny/pn6sXfXwUAFJNstu3cAzdUriT3k9fLJzt2c5f4b+3Oco1QNSptnSNbQ0WMHDrAsuOB7To/2blHbbZFn6Wt72fZRgmCIAiCIIoR9hZRP0xU7eBNn//eCtW6n0oc5fRfP3NXK1URii74ah9MUhWhIwb3s8Qaim06H3vmJdi2Y7eoUxJ+XiARShAEQRCE3ZQYIYpERkSgMvyWN33BeytU638W5+jxE4piLvuWxkJXvOd8LlRdPQnKZO4XikIUoc0a1Te972eysuV4UByvCoP3bt3wjOkNEgRBEARBqFCihKhc1snn6wwAvyuJvr/O5sKzE6foFqNKFJSO5k6LPnMCbl04DqLOcEudQpky0fDoQyMsEaG4P48+/aJQhEqS9DsAtNi7dQOVZyIIgiAIwhFKlBBFMPFGkqAFE16XcTY3F96ZvUB2Y2sBBaMSVyh0RkKuPLUfqq2ZCJ5z/C5JuM5nJzwCtWvebXp/9331jSyucb944LHAY7J364ZtpjdIEARBEAShkRInRIHVGAWAQbzpaDlEN7YWMVqlsnJd0NidS6DUn2f+kfke89NOqLpqInjyz3JrJd1QKRZeee5J7nr1sH3nHnjlzelw9ix/e+DzfXvu3LkqVJ6JIAiCIAinkXx29o10OXVbth0EPt9c3iibNW4AI4f0F+7ER5u3woIlKxSnYTvPv2JvleuIXvNzOkT/ynfFI7fdchM8OnoEXFGmjOkDh6WZRBn9jLXnL1zo99WOrb+Z3iBBEARBEIROSrQQReq0aLNAAombId6udUvo3zuRuzy6vEeOfQLyNLryeTRtVB8esKBEE1pxsQKAmgj1gW/h5x9vEqtsgiAIgiAIGymRrvlAUIyhKONNR4vndkHNTbReJiZ0MDWGbp3bWyZC/zPxddi2Yxe3371PztmHsSRCCYIgCIIINiVeiCIXLlwc7QPftzzhNm32PPjiy6+5y6PVtEnDekLxx/sZMaS/aSGLHDt+QhahR48L3f+/+3wwaO/HG6eY3iBBEARBEIRJSrxr3s+9jVteGxkZsR0A7lKafkV0GXjuibFyByYe85Ysh/Wbt2raXpnoaHju8bFQ5YbKpscui9CXX4ezefzMeCZCm3+xbdNXpjdIEARBEARhASREA6jdvM29kgRYwugapekoRl998Wn413XluOv4Yf9BWLF6Lfy4X7l8EwrQ2jXvgYF9kixJSsJWndNS5qnNhkX8B5AIJQiCIAjCTZAQLQaKUQDgljLCskrPPzGOWz/Uz/9+PSOLUvzt5/Zbb4Y7br3ZsrF+tGmrbIVV4duLFy82pcx4giAIgiDcBglRBWo3bzMQALgdhm6/5SZ47olxwRwivDN7vtwzXoWFFy9eHE0ilCAIgiAIN0JClEPt5m0eBgBuUk/zxg0syXTXC2bGT37zHdnaqsLbX2zbNNrxARIEQRAEQWiEhKiA2s3bLACAy2qM+o9ZUpeOkNjZfMa7VnLz8uCZl1+Fn4/xe8ZDUcvOQV9s26QaOEoQBEEQBBFMSIiqUKtZ6w8AoCNvrlHJA6FZowa2j+PoiRPwzEuvCXvGF2XG+wamb9+y2vYBEQRBEARBmITqiKpQUFDQn2WdKzI1ZZ4sEu3kh/0HNIlQSZKakwglCIIgCCJUIIuoBrDGqNfrPcot61SmDDz/5DioUunyGqOsk5FhsDzT1FncvCn/Vr4pLPR1/mrH1qN2HwuCIAiCIAirICGqESzrVFhYyK0x+n//ug5ef/EZ1bJOepi/eDms3ZSmtsT2wsLCzpQZTxAEQRBEqEFCVAe1mrVu7vP5PuYtceMNlTXVGNUClmf6+NNdanMuTN++hXrGEwRBEAQRklCMqA72fbIZLaKDeEv8fOy43JfeDFieaeyTz8PHn+6UHfuCz3MkQgmCIAiCCGXIImqAuKbxbwIAt0Zn88YN4cHkgbpX/OuZLJj0xjtw9PhxlTmlQenbt1B5JoIgCIIgQhqyiBogfXvaGABYwFty245dcpKRHo4dPwFjn3hOTYT+Xljou5dEKEEQBEEQ4QBZRE0Q1zT+awC4m7eGUcMGQ7NG9VU3gKL13YVL4WyesDzTMZYZ/7WjO0kQBEEQBGETEXRgjVNY6GsmSdInPDE6d+FSKBMdBXXi7uVuY93GNHh38TK1MXzj8/maUWY8QRAEQRDhBFlETXJv45ZVJEn6mlfWCWnRpCHUibvnkiDFhKTvf9ovi9DvfzqgNoAPfD7fABKhBEEQBEGEGyRELeDexi3vYZZRrhg1giTBgvTtaQNC5DAQBEEQBEHogoSoRdzbuCUKRiuTiAZ+tWPr/KDuFEEQBEEQhI1Q1rxFMNGov2bT5fxOIpQgCIIgiJIAWUQt5t7GLTtjd06DbvpjAECZ8QRBEARBlAjIImoxX+3YugYA7hHVGeXwFi5HIpQgCIIgiJICWURtBDPqAQCL3zfjlHjaDgAoXNd8tWPr0bDZcYIgCIIgCDUA4P8Bug5L3XKIdU0AAAAASUVORK5CYII=",
                                    width: 200,
                                    margin: [0, 0, 0, 50],
                                    alignment: 'center'
                                },
                                {text: 'Patient Info', style: 'titles'},
                                {text: 'Name: ' + item.name, style: 'header'},
                                {text: 'Age: ' + item.age, style: 'header'},
                                {
                                    text: 'Date: ' + moment.unix(item.date.seconds).format('YYYY-MM-DD HH:mm a'),
                                    style: 'header'
                                },
                                {text: 'Drugs', style: 'titles'},
                                {
                                    ul: item.drugs.map((drug, i) => {
                                        return drug.label
                                    }),
                                    style: 'drugs'
                                }
                            ],

                            styles: {
                                header: {
                                    fontSize: 12,
                                    bold: true,
                                    margin: [0, 0, 0, 10],
                                    color: "#455056"
                                },
                                drugs: {
                                    color: "#455056",
                                    markerColor: "#0ab4ec"
                                },
                                titles: {
                                    fontSize: 16,
                                    color: "#0ab4ec",
                                    margin: [0, 0, 0, 10],
                                    bold: true,
                                }
                            }
                        };
                        // var win = window.open('/file', '_blank');
                        // pdfMake.createPdf(pdf_page).open({}, win);
                        pdfMake.createPdf(pdf_page).download(item.name + '_prescription');

                    },
                    findPre: (type, search) => {
                        this.findPrescription(type, search);
                    }

                }
            }}>
                <Nav/>

                <section className="section">
                    <div className="container">
                        <h1 className="title">All Prescription</h1>
                        <List/>
                    </div>
                </section>
            </Context.Provider>
        )
    }
}

ReactDOM.render(<App/>, document.getElementById("root"));