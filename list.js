import React, {Component} from 'react';
import Context from '/Context';
import moment from 'moment';

class List extends Component {
    render() {
        return (
                <Context.Consumer>
                {
                    (ctx) => {
                        return (
                            <table className="table is-fullwidth is-striped">
                                <thead>
                                <tr key="00">
                                    <th>#</th>
                                    <th>Patient Name</th>
                                    <th>Patient Age</th>
                                    <th>Date</th>
                                    <th>Number of Drugs</th>
                                    <th>Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                {
                                    ctx.state.data.map((item, i)=>{
                                        // console.log(item.date.seconds);
                                        return(
                                            <tr key={i++}>
                                                <td width="5%">#{i}</td>
                                                <td width="25%">{item.name}</td>
                                                <td width="10%">{item.age}</td>
                                                <td width="25%">{moment.unix(item.date.seconds).format('YYYY-MM-DD HH:mm a')}</td>
                                                <td width="20%">{item.drugs.length}</td>
                                                <td width="15%"><button className="button is-primary is-outlined" onClick={ctx.actions.exportPDF.bind(this,item)}>Export PDF</button></td>
                                            </tr>
                                        )
                                    })
                                }
                                </tbody>
                            </table>
                        )
                    }
                }
            </Context.Consumer>
        )
    }
}


export default List;